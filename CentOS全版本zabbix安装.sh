#!/bin/bash

# 纯干货笔记
# 适用范围，采用CentOS MiniInstall安装的所有版本
# 先修改网卡配置，并执行下面3行（也可以 nmtui-edit）
# systemctl set-default multi-user.target
# yum install sshd
# systemctl enable sshd.service

# 启动菜单
# grub2-mkconfig -o /boot/grub2/grub.cfg
# grub2-mkconfig -o /boot/efi/EFI/redhat/grub.cfg

# 先把需要的源全都下载回来，此部分用于生成安装包
# 主要就是靠下面这行
# yum install -q --downloadonly --downloaddir=/mdata

# 先格式化新磁盘
# mkfs.xfs /dev/sdb
# 建立挂载点
# mkdir -p /mdata/packages
# 映射挂载
# mount -t xfs /dev/sdb /mdata
# 删除挂载
# umount /dev/sdb

# 查看磁盘分区状态
# lsblk
# 查看磁盘大小
# df -hT

# 挂载磁盘到启动
# vi /etc/fstab
# blkid
# blkid /dev/sdb |cut -f2 -d '"'

# 查看分区
# lvs
# 查看分区详细
# lvdisplay
# df -hl
# 修改磁盘分区
# umount /home
# lvremove /dev/mapper/cs-home
# lvextend -L +355G /dev/mapper/cs-root
# xfs_growfs /dev/cs/root
# lvcreate -n home -l 100%free cs
# mkfs.xfs /dev/cs/home
# mount /dev/mapper/cs-home
# df -hl

# Redhat 需要先注册
# subscription-manager register
 
cat >> /etc/fstab <-EOF
UUID=xxxx    /etomorrow    xfs    defaults    0 0
EOF

# 禁用selinux
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

# 防火墙
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --zone=public --add-port=81/tcp --permanent
firewall-cmd --zone=public --add-port=3000/tcp --permanent
firewall-cmd --zone=public --add-port=3306/tcp --permanent
firewall-cmd --zone=public --add-port=10050/tcp --permanent
firewall-cmd --zone=public --add-port=10051/tcp --permanent
firewall-cmd --reload

# 关闭内部邮件
systemctl stop postfix

# 安装需要的源

# 开始产生分支
# centos7安装
mv /etc/yum.repos.d /etc/yum.repos.bak
mkdir -p /etc/yum.repos.d
# 腾讯云源（注意，两个文件名不一样！）
# wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.cloud.tencent.com/repo/centos7_base.repo
# 阿里云源
# wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
# centos7需要换mysql源
cat > /etc/yum.repos.d/centos7_base.repo <<-EOF
[os]
name=Qcloud centos os - $basearch
baseurl=http://mirrors.cloud.tencent.com/centos/7/os/x86_64/
enabled=1
gpgcheck=1
gpgkey=http://mirrors.cloud.tencent.com/centos/RPM-GPG-KEY-CentOS-7

[updates]
name=Qcloud centos os - $basearch
baseurl=http://mirrors.cloud.tencent.com/centos/7/updates/x86_64/
enabled=1
gpgcheck=1
gpgkey=http://mirrors.cloud.tencent.com/centos/RPM-GPG-KEY-CentOS-7

[extras]
name=Qcloud centos extras - $basearch
baseurl=http://mirrors.cloud.tencent.com/centos/7/extras/x86_64/
enabled=1
gpgcheck=1
gpgkey=http://mirrors.cloud.tencent.com/centos/RPM-GPG-KEY-CentOS-7

[Mysql]
name=Mysql 8.0 release
metadata_expire=-1
gpgcheck=0
enabled=1
baseurl=https://mirrors.cloud.tencent.com/mysql/yum/mysql-8.0-community-el7-x86_64/
#baseurl=https://mirrors.cloud.tencent.com/mysql/yum/mysql-8.0-community-el7-x86_64/

[epel]
name=centos 7 epel release
metadata_expire=-1
gpgcheck=0
enabled=1
baseurl=https://mirrors.cloud.tencent.com/epel/7/x86_64/

[remi-safe]
name=remi safe release
metadata_expire=-1
enabled=1
gpgcheck=0
mirrorlist=http://cdn.remirepo.net/enterprise/7/safe/mirror

[remi-php80]
name=remi php 8.0 release
metadata_expire=-1
enabled=1
gpgcheck=0
mirrorlist=http://cdn.remirepo.net/enterprise/7/php80/mirror

EOF

# 跳到192行
# 腾讯云源
# https://mirrors.cloud.tencent.com/
# 阿里云源
# https://mirrors.aliyun.com/

# centos8安装
# 阿里云源
mv /etc/yum.repos.d /etc/yum.repos.bak
mkdir -p /etc/yum.repos.d

cat > /etc/yum.repos.d/centos.repo <<-EOF
[BaseOS]
name=CentOS 8.0 Stream BaseOS release
metadata_expire=-1
gpgcheck=0
enabled=1
baseurl=https://mirrors.aliyun.com/centos/8-stream/BaseOS/x86_64/os/

[AppStream]
name=CentOS 8.0 Stream AppStream release
metadata_expire=-1
gpgcheck=0
enabled=1
baseurl=https://mirrors.aliyun.com/centos/8-stream/AppStream/x86_64/os/

[Extras]
name=CentOS 8.0 Stream Extras release
metadata_expire=-1
gpgcheck=0
enabled=1
baseurl=https://mirrors.aliyun.com/centos/8-stream/extras/x86_64/os/

EOF
# 换成腾讯云源
# sed -i 's/mirrors.aliyun.com/mirrors.cloud.tencent.com/g' /etc/yum.repos.d/centos.repo

# centos9（仅阿里云源）
sed -i 's/metalink=https:\/\/mirrors.centos.org\/metalink?repo=centos-baseos-$stream&arch=$basearch&protocol=https,http/baseurl=https:\/\/mirrors.aliyun.com\/centos-stream\/9-stream\/BaseOS\/$basearch\/os\//g' /etc/yum.repos.d/centos.repo
sed -i 's/metalink=https:\/\/mirrors.centos.org\/metalink?repo=centos-appstream-$stream&arch=$basearch&protocol=https,http/baseurl=https:\/\/mirrors.aliyun.com\/centos-stream\/9-stream\/AppStream\/$basearch\/os\//g' /etc/yum.repos.d/centos.repo
mv /etc/yum.repos.d /etc/yum.repos.bak
mkdir -p /etc/yum.repos.d

cat > /etc/yum.repos.d/centos.repo <<-EOF
[dvd-BaseOS]
name=Red Hat Enterprise Linux 9 - BaseOS
metadata_expire=-1
gpgcheck=0
enabled=1
baseurl=https:///mirrors.aliyun.com/centos-stream/9-stream/BaseOS/x86_64/os/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial

[dvd-AppStream]
name=Red Hat Enterprise Linux 9 - AppStream
metadata_expire=-1
gpgcheck=0
enabled=1
baseurl=https:///mirrors.aliyun.com/centos-stream/9-stream/AppStream/x86_64/os/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial

EOF

# centos9本地源，写本地源repo配置
cat > /etc/yum.repos.d/dvd.repo <<-EOF
[dvd-BaseOS]
name=Red Hat Enterprise Linux 9 - BaseOS
metadata_expire=-1
gpgcheck=0
enabled=1
baseurl=file:///mnt/dvd//BaseOS/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial

[dvd-AppStream]
name=Red Hat Enterprise Linux 9 - AppStream
metadata_expire=-1
gpgcheck=0
enabled=1
baseurl=file:///mnt/dvd//AppStream/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial

EOF

# yum clean all

# 安装必要的支持库
# zabbix: unixODBC OpenIPMI unixODBC-devel OpenIPMI-devel fping golang
# grafana: chkconfig urw-fonts

yum install -y yum-utils wget zip vim net-tools sysstat epel-release fping gcc gcc-c++ unixODBC-devel mysql-devel libcurl libcurl-devel libevent libevent-devel curl-devel libxml2  libxml2-devel chkconfig urw-fonts
# 安装nginx 1.20.1 mysql 8.0.33
yum -y install nginx nginx-all-modules mysql mysql-server

# centos7特别处理
# remi源安装
# yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm
# # 默认安装路径/opt/remi/php80/root
yum install --installroot=/usr/share/php/ --releasever=/ --enablerepo=remi-php80 php
yum install php-devel php-common php-fpm php-gd php-ldap php-bcmath php-mbstring php-mysqlnd php-odbc php-opcache php-pdo php-pear php-pecl-zip php-xml php-soap php-process php-snmp
# yum remove -y remi-release

# 安装php8.0
# centos9下不需要switch
# dnf -y module switch-to php:8.0
yum install -y php-devel php-common php-fpm php-gd php-ldap php-bcmath php-mbstring php-mysqlnd php-odbc php-opcache php-pdo php-pear php-pecl-zip php-xml php-soap php-process php-snmp

# zabbix直接安装

# centos7
# yum install -y zabbix6.0-server zabbix6.0-server-mysql zabbix6.0-selinux zabbix6.0-dbfiles-mysql zabbix6.0-agent zabbix6.0-web zabbix6.0-web-mysql
# 安装中文字体
# yum install -y google-noto-cjk-fonts

# centos8
# rpm -Uvh https://mirrors.aliyun.com/zabbix/zabbix/6.2/rhel/8/x86_64/zabbix-release-latest.el8.noarch.rpm
# centos9
# rpm -Uvh https://mirrors.aliyun.com/zabbix/zabbix/6.4/rhel/9/x86_64/zabbix-release-latest.el9.noarch.rpm

dnf install -y zabbix-server-mysql zabbix-sql-scripts zabbix-selinux-policy zabbix-web-mysql zabbix-nginx-conf zabbix-agent

# grafana安装
yum install -y https://mirrors.cloud.tencent.com/grafana/yum/rpm/Packages/grafana-9.5.2-1.x86_64.rpm
# 安装两个插件
grafana cli plugins install alexanderzobnin-zabbix-app
grafana cli plugins install grafana-clock-panel
# 本地安装插件
# grafana cli --pluginUrl ./alexanderzobnin-zabbix-app-4.3.1.zip plugins install alexanderzobnin-zabbix-app
# grafana cli --pluginUrl ./grafana-clock-panel-2.1.3.any.zip plugins install grafana-clock-panel
# 配置grafana
sed -i 's/^;type = sqlite3/type = mysql/g' /etc/grafana/grafana.ini
sed -i 's/^;host = 127.0.0.1:3306/host = localhost:3306/g' /etc/grafana/grafana.ini
sed -i 's/^;name = grafana/name = gra_data/g' /etc/grafana/grafana.ini
sed -i 's/^;user = root/user = zabbix/g' /etc/grafana/grafana.ini
sed -i '0,/^;password =/s//password = """mY123#pa4s"""/' /etc/grafana/grafana.ini
sed -i 's/^;logs = \/var\/log\/grafana/logs = \/etomorror\/logs\//g' /etc/grafana/grafana.ini


#
# CENTOS8以上加入中文支持
# yum install -y langpacks-zh_CN

# 安装MSSQL SERVER驱动
# 默认安装FreeTDS，源在epel-release里
yum install -y freetds freetds-devel freetds-libs

# 微软sqlserver驱动
# https://packages.microsoft.com/yumrepos/mssql-rhel8-release/Packages/m/msodbcsql-13.0.1.0-1.x86_64.rpm

# Centos7版本
# curl https://packages.microsoft.com/config/centos/7/prod.repo > /etc/yum.repos.d/mssql-release.repo
# Centos8版本
# curl https://packages.microsoft.com/config/centos/8/prod.repo > /etc/yum.repos.d/mssql-release.repo
# Centos9版本
# curl https://packages.microsoft.com/config/rhel/9.0/prod.repo > /etc/yum.repos.d/mssql-release.repo
# 驱动的版本
# ACCEPT_EULA=Y yum install -y msodbcsql13
# ACCEPT_EULA=Y yum install -y msodbcsql17
# ACCEPT_EULA=Y yum install -y msodbcsql18
# 防止提示
# unalias cp
# cp /opt/microsoft/msodbcsql18/lib64/libmsodbcsql-18.1.so.1.1 /usr/lib64/

# Oracle驱动
# 适用于oracle旧版
# yum install -y https://download.oracle.com/otn/linux/instantclient/122010/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
# yum install -y https://download.oracle.com/otn/linux/instantclient/122010/oracle-instantclient12.2-odbc-12.2.0.1.0-2.x86_64.rpm
# 通常装这个版本，小心兼容11g
yum install -y https://download.oracle.com/otn_software/linux/instantclient/1917000/oracle-instantclient19.17-basic-19.17.0.0.0-1.x86_64.rpm
yum install -y https://download.oracle.com/otn_software/linux/instantclient/1917000/oracle-instantclient19.17-odbc-19.17.0.0.0-1.x86_64.rpm
ln -s /usr/lib/oracle/19.17/client64/lib/libsqora.so.19.1 /usr/lib64/libsqora.so.19.1
ldconfig

# mysql 驱动
# centos7
# yum install -y mysql-connector-odbc
yum install -y mariadb-connector-odbc

# 源删除（一般不删除）
# yum remove -y epel-release

# 建立组
# 删除系统自建组
groupadd -g 888 www
usermod -a -G www -g 888 nginx
usermod -a -G www -g 888 mysql
usermod -a -G www -g 888 grafana
usermod -a -G www -g 888 zabbix

groupdel mysql
groupdel nginx
groupdel grafana
groupdel apache
groupdel zabbix
groupdel zabbixsrv

userdel -r apache
userdel -r zabbixsrv

rm -rf /usr/share/httpd/

# 建立/etomorrow目录并授权
mkdir -p /etomorrow/{database/mysql,logs}
chown -R root:www /etomorrow
chmod 775 -R /etomorrow

# 修改服务相关配置
# nginx
# 改成友好的方式
# sed -i 's/ExecStart=\/usr\/sbin\/nginx$/ExecStart=\/usr\/sbin\/nginx -c \/etc\/nginx\/nginx.conf/g' /usr/lib/systemd/system/nginx.service
# 11行插入nginx配置文件
sed -i '11iEnvironment="CONFFILE=/etc/nginx/nginx.conf"' /usr/lib/systemd/system/nginx.service
sed -i '12iEnvironmentFile=-/etc/sysconfig/nginx"' /usr/lib/systemd/system/nginx.service
# mysql
sed -i 's/Group=mysql/Group=www/g' /usr/lib/systemd/system/mysqld.service
sed -i 's/TimeoutSec=0/TimeoutSec=10/g' /usr/lib/systemd/system/mysqld.service
# grafana
sed -i 's/Group=grafana/Group=www/g' /usr/lib/systemd/system/grafana-server.service

# 在centos7上还需要修改两个服务文件
# 注意Group=zabbix需要修改

cat > /usr/lib/systemd/system/zabbix-agent.service <<-EOF
[Unit]
Description=Zabbix Agent
After=syslog.target
After=network.target

[Service]
Environment="CONFFILE=/etc/zabbix/zabbix_agentd.conf"
EnvironmentFile=-/etc/sysconfig/zabbix-agent
Type=forking
Restart=on-failure
PIDFile=/run/zabbix/zabbix_agentd.pid
KillMode=control-group
ExecStart=/usr/sbin/zabbix_agentd -c \$CONFFILE
ExecStop=/bin/kill -SIGTERM \$MAINPID
RestartSec=10s
User=zabbix
Group=www

[Install]
WantedBy=multi-user.target

EOF

cat > /usr/lib/systemd/system/zabbix-server.service <<-EOF
[Unit]
Description=Zabbix Server
After=syslog.target
After=network.target
After=mysql.service
After=mysqld.service

[Service]
Environment="CONFFILE=/etc/zabbix/zabbix_server.conf"
EnvironmentFile=-/etc/sysconfig/zabbix-server
Type=forking
Restart=on-failure
PIDFile=/run/zabbix/zabbix_server.pid
KillMode=control-group
ExecStart=/usr/sbin/zabbix_server -c \$CONFFILE
ExecStop=/bin/kill -SIGTERM \$MAINPID
RestartSec=10s
TimeoutSec=10

[Install]
WantedBy=multi-user.target

EOF

# 写配置
# 写入配置 /etc/odbcinst.ini
# 最后一行用于防止中文乱码
cat >> /etc/odbcinst.ini <<-EOF
[OracleDB19]
Description     = ODBC for Oracle 19.17
Driver          = /usr/lib64/libsqora.so.19.1
Setup           =
FileUsage       = 1
CPTimeout       =
CPReuse         =
DMEnvAttr       = SQL_ATTR_UNIXODBC_ENVATTR={NLS_LANG=SIMPLIFIED CHINESE.AL32UTF8}

# Trace = Yes
# TraceFile = /tmp/sql.log
# ForceTrace = Yes
# FileUsage = 1
# MStmtAttr=SQL_QUERY_TIMEOUT=10;SQL_ATTR_LOGIN_TIMEOUT=10;
# CPTimeout=1000
# CPTimeToLive=10

EOF

# nginx相关配置
rm -f /etc/nginx/conf.d/php-fpm.conf
rm -f /etc/php-fpm.d/www.conf
# vim /etc/nginx.conf

cat > /etc/nginx/nginx.conf <<-EOF
user nginx www;
worker_processes auto;
error_log /etomorrow/logs/nginx.error.log error;
pid /run/nginx.pid;
worker_rlimit_nofile 65535;

include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "\$http_referer" '
                      '"\$http_user_agent" "\$http_x_forwarded_for"';

    sendfile            on;
    tcp_nopush          on;
    gzip                on;
    keepalive_timeout   65;
    types_hash_max_size 4096;
    client_max_body_size 20M;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    include /etc/nginx/conf.d/*.conf;

}

EOF

cat > /etc/nginx/conf.d/etomonitor.conf <<-EOF
server {
    listen       80;
    server_name  etomonitor;
    root   /usr/share/etomonitor/public;
    index  index.php index.html index.htm;
    charset utf-8;
    
    access_log      /etomorrow/logs/etomonitor.log main;

    error_page   500 502 503 504  /50x.html;

    autoindex off;
    location / {
        if (!-e \$request_filename) {
            rewrite  ^(.*)\$  /index.php?s=/\$1  last;
        }
    }
    location ~ \.php\$ {
        fastcgi_pass   unix:/run/php-fpm/zabbix.sock;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
        set \$path_info \$fastcgi_path_info;
        fastcgi_param  PATH_INFO \$path_info;
        include        fastcgi_params;
    }
}

EOF

cat > /etc/nginx/conf.d/zabbix.conf <<-EOF
server {
    listen          81;
    server_name     zabbix;
    root    /usr/share/zabbix;

    access_log  /etomorrow/logs/zabbix_web.log main;

    large_client_header_buffers 8 8k;
    
    client_max_body_size 10M;

    index   index.php;

    location = /favicon.ico {
        log_not_found   off;
    }

    location / {
        try_files       \$uri \$uri/ =404;
    }

    location /assets {
        access_log      off;
        expires         10d;
    }

    location ~ /\.ht {
        deny            all;
    }

    location ~ /(api\/|conf[^\.]|include|locale) {
        deny            all;
        return          404;
    }

    location /vendor {
        deny            all;
        return          404;
    }
    location ~ /\. {
        deny all;
        access_log off;
        log_not_found off;
    }

    # caching of files
    location ~* \.ico\$ {
        expires 1y;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|xml|txt)\$ {
        expires 14d;
    }

    location ~ /(app\/|conf[^\.]|include\/|local\/|locale\/) {
        deny all;
        return 404;
    }

    location ~ [^/]\.php(/|\$) {
            fastcgi_pass    unix:/run/php-fpm/zabbix.sock;
            fastcgi_index   index.php;

            fastcgi_param   SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
            fastcgi_param   PATH_TRANSLATED \$document_root\$fastcgi_script_name;
            include         fastcgi_params;
            fastcgi_param   QUERY_STRING    \$query_string;
            fastcgi_param   REQUEST_METHOD  \$request_method;
            fastcgi_param   CONTENT_TYPE    \$content_type;
            fastcgi_param   CONTENT_LENGTH  \$content_length;

            fastcgi_intercept_errors        on;
            fastcgi_ignore_client_abort     off;
            fastcgi_connect_timeout         60;
            fastcgi_send_timeout            180;
            fastcgi_read_timeout            180;
            fastcgi_buffer_size             128k;
            fastcgi_buffers                 4 256k;
            fastcgi_busy_buffers_size       256k;
            fastcgi_temp_file_write_size    256k;
    }
}

EOF

# vim /etc/php.ini
# extension=msgpack.so
# PHP优化
# vim /etc/php-fpm.d/zabbix.conf

cat > /etc/php-fpm.d/zabbix.conf <<-EOF

[zabbix]
user = nginx
group = www

listen = /run/php-fpm/zabbix.sock
listen.acl_users = nginx
listen.allowed_clients = 127.0.0.1

pm = dynamic
pm.max_children = 50
pm.start_servers = 5
pm.min_spare_servers = 5
pm.max_spare_servers = 35
pm.max_requests = 200

php_value[session.save_handler] = files
php_value[session.save_path]    = /tmp/session

php_value[max_execution_time] = 300
php_value[memory_limit] = 256M
php_value[post_max_size] = 16M
php_value[upload_max_filesize] = 20M
php_value[max_input_time] = 300
php_value[max_input_vars] = 1000
php_value[date.timezone] = Asia/Shanghai

EOF

# vim /etc/php-fpm.conf
sed -i 's/^error_log = \/var\/log\/php-fpm\/error.log/error_log = \/etomorrow\/logs\/php-fpm-error.log/g' /etc/php-fpm.conf
sed -i 's/^pid = \/var\/run\/php-fpm\/php-fpm.pid/pid = \/run\/php-fpm\/php-fpm.pid/g' /etc/php-fpm.conf

# vim /etc/php.ini
sed -i 's/post_max_size = 8M/post_max_size = 16M/g' /etc/php.ini
sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 20M/g' /etc/php.ini
sed -i 's/;date.timezone =/date.timezone = PRC/' /etc/php.ini
sed -i 's/max_execution_time = 30/max_execution_time = 300/g' /etc/php.ini
sed -i 's/max_input_time = 60/max_input_time = 300/g' /etc/php.ini
sed -i 's/memory_limit = 128M/memory_limit = 256M/g' /etc/php.ini

# 配置zabbix
# centos7下要转移一下目录
mv /etc/zabbix*.conf /etc/zabbix/
# 建zabbix相关目录
mkdir -p /usr/lib/zabbix/{alertscripts,externalscripts,modules,ssl/keys,ssl/certs,ssl/ssl_ca}

# 修改zabbix agent配置
sed -i 's/# LogType=file/LogType=file/g' /etc/zabbix/zabbix_agentd.conf
sed -i 's/LogFileSize=0/LogFileSize=1000/g' /etc/zabbix/zabbix_agentd.conf
sed -i 's/LogFile=\/var\/log\/zabbix\/zabbix_agentd.log/LogFile=\/etomorrow\/logs\/zabbix_agentd.log/g' /etc/zabbix/zabbix_agentd.conf

#修改zabbix server配置
sed -i 's/# LogType=file/LogType=file/g' /etc/zabbix/zabbix_server.conf
sed -i 's/LogFileSize=0/LogFileSize=1000/g' /etc/zabbix/zabbix_server.conf
sed -i 's/DBName=zabbix/#DBName=zabbix/g' /etc/zabbix/zabbix_server.conf
sed -i 's/DBUser=zabbix/#DBUser=zabbix/g' /etc/zabbix/zabbix_server.conf
sed -i 's/LogFile=\/var\/log\/zabbixsrv\/zabbix_server.log/LogFile=\/etomorrow\/logs\/zabbix_server.log/g' /etc/zabbix/zabbix_server.conf
sed -i 's/^TmpDir=/#TmpDir=/g' /etc/zabbix/zabbix_server.conf
sed -i 's/^# TmpDir=/TmpDir=/g' /etc/zabbix/zabbix_server.conf
sed -i 's/\/var\/lib\/zabbixsrv/\/usr\/lib\/zabbix/g' /etc/zabbix/zabbix_server.conf
sed -i 's/^PidFile=\/run\/zabbixsrv/PidFile=\/run\/zabbix/g' /etc/zabbix/zabbix_server.conf
sed -i 's/^DBSocket=\/var\/lib\/mysql\/mysql.sock/DBSocket=\/etomorrow\/database\/mysql\/mysql.sock/g' /etc/zabbix/zabbix_server.conf
# centos7上如果支持openssl1.0以下则修改PSK加密方式
sed -i 's/^# TLSCipherPSK=/TLSCipherPSK=RSA+aRSA+AES128/g' /etc/zabbix/zabbix_server.conf

# DBSocket=/var/lib/mysql/mysql.sock
# /etomorrow/database/mysql/mysql.sock

# vim /etc/zabbix/zabbix-server.conf
cat >> /etc/zabbix/zabbix_server.conf <<-EOF

ListenPort=10051
DBHost=127.0.0.1
DBName=zbx_data
DBUser=zabbix
DBPassword=mY123#pa4s

User=zabbix

CacheSize=256M
CacheUpdateFrequency=60
HousekeepingFrequency=1
ListenBacklog=512
StartODBCPollers=50
StartPollers=20
StartPreprocessors=20
TrendCacheSize=64M
TrendFunctionCacheSize=128M
ValueCacheSize=128M

AlertScriptsPath=/usr/lib/zabbix/alertscripts
ExternalScripts=/usr/lib/zabbix/externalscripts
LoadModulePath=/usr/lib/zabbix/modules/
FpingLocation=/usr/sbin/fping
SSHKeyLocation=/usr/lib/zabbix/ssh_keys
FpingLocation=/usr/sbin/fping
SSLCertLocation=/usr/lib/zabbix/ssl/certs/
SSLKeyLocation=/usr/lib/zabbix/ssl/keys/
SSLCALocation=/usr/lib/zabbix/ssl/ssl_ca/

EOF

# 配置zabbix-web
cat > /etc/zabbix/web/zabbix.conf.php  <<-EOF
<?php
// Zabbix GUI configuration file.

\$DB['TYPE']            = 'MYSQL';
\$DB['SERVER']          = '127.0.0.1';
\$DB['PORT']            = '3306';
\$DB['DATABASE']        = 'zbx_data';
\$DB['USER']            = 'zabbix';
\$DB['PASSWORD']        = 'mY123#pa4s';

// Schema name. Used for PostgreSQL.
\$DB['SCHEMA']		= '';

// Used for TLS connection.
\$DB['ENCRYPTION']		= false;
\$DB['KEY_FILE']			= '';
\$DB['CERT_FILE']		= '';
\$DB['CA_FILE']			= '';
\$DB['VERIFY_HOST']		= false;
\$DB['CIPHER_LIST']		= '';

// Vault configuration. Used if database credentials are stored in Vault secrets manager.
\$DB['VAULT']			= '';
\$DB['VAULT_URL']		= '';
\$DB['VAULT_DB_PATH']		= '';
\$DB['VAULT_TOKEN']		= '';
\$DB['VAULT_CERT_FILE']		= '';
\$DB['VAULT_KEY_FILE']		= '';
// Uncomment to bypass local caching of credentials.
// \$DB['VAULT_CACHE']		= true;

// Use IEEE754 compatible value range for 64-bit Numeric (float) history values.
// This option is enabled by default for new Zabbix installations.
// For upgraded installations, please read database upgrade notes before enabling this option.
\$DB['DOUBLE_IEEE754']		= false;

// Uncomment and set to desired values to override Zabbix hostname/IP and port.
// \$ZBX_SERVER			= '';
// \$ZBX_SERVER_PORT		= '';

\$ZBX_SERVER_NAME		= 'server160';

\$IMAGE_FORMAT_DEFAULT	= IMAGE_FORMAT_PNG;

// Uncomment this block only if you are using Elasticsearch.
// Elasticsearch url (can be string if same url is used for all types).
//\$HISTORY['url'] = [
//	'uint' => 'http://localhost:9200',
//	'text' => 'http://localhost:9200'
//];
// Value types stored in Elasticsearch.
//\$HISTORY['types'] = ['uint', 'text'];

// Used for SAML authentication.
// Uncomment to override the default paths to SP private key, SP and IdP X.509 certificates, and to set extra settings.
//\$SSO['SP_KEY']			= 'conf/certs/sp.key';
//\$SSO['SP_CERT']			= 'conf/certs/sp.crt';
//\$SSO['IDP_CERT']		= 'conf/certs/idp.crt';
//\$SSO['SETTINGS']		= [];

EOF

# 配置mysql8.0
cat > /etc/my.cnf <<-EOF
[client]
port                            = 3306
default-character-set           = utf8mb4
socket                          = /tmp/mysql.sock

[mysqld]
server-id                       = 1
user                            = mysql
port                            = 3306
sql_mode                        = "STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION"

datadir                         = /etomorrow/database/mysql
socket                          = /tmp/mysql.sock
pid-file                        = /run/mysqld/mysqld.pid

default_storage_engine          = InnoDB
character_set_server            = utf8mb4
collation_server                = utf8mb4_unicode_ci
init_connect                    = "SET NAMES utf8mb4"
lower_case_table_names          = 1

max_connections                 = 2000

default-time-zone               = system

skip_name_resolve               = OFF
interactive_timeout             = 300
wait_timeout                    = 300
max_allowed_packet              = 256MB
lock_wait_timeout               = 300
# 官方建议innodb_buffer_pool_size 值为物理内存总大小的80%
innodb_buffer_pool_size         = 4096MB
innodb_thread_concurrency       = 0
innodb_log_buffer_size          = 256MB
innodb_doublewrite              = 1
innodb_log_file_size            = 512MB
innodb_log_files_in_group       = 3

innodb_max_dirty_pages_pct      = 90
innodb_flush_log_at_trx_commit  = 1
innodb_lock_wait_timeout        = 300
innodb_lru_scan_depth           = 256

slow_query_log                  = ON
long_query_time                 = 10 
slow_query_log_file             = /etomorrow/logs/mysql.slow.log

#binlog_format                   = ROW
log-bin                         = /etomorrow/database/mysql/binlog
log-error                       = /etomorrow/logs/mysql.error.log
max_binlog_size                 = 500MB

skip-character-set-client-handshake     = ON
explicit-defaults-for-timestamp         = ON

innodb_flush_method             = O_DIRECT
innodb_autoinc_lock_mode        = 2
innodb_io_capacity_max          = 3000

join_buffer_size                = 16M
sort_buffer_size                = 16M
thread_cache_size               = 2250
back_log                        = 500

# 即将过期
# expire_logs_days              = 7
# default_authentication_plugin = mysql_native_password
# max_open_files                  = 10010
# table_open_cache                = 4000

[mysql]
default-character-set           = utf8mb4

[mysqldump]
quick
max_allowed_packet              = 256MB 

EOF

# 设置文件夹权限
mkdir -p /run/php-fpm
chown -R mysql:www /var/lib/mysql-files
chown -R mysql:www /var/lib/mysql-keyring
chown -R grafana:www /var/lib/grafana
chown -R grafana:www /etc/grafana
chown -R mysql:www /run/mysqld
chown -R zabbix:www /run/zabbix
chown -R nginx:www /run/php-fpm
chown -R root:www /etc/zabbix/zabbix_server.conf
chown -R nginx:www /etc/zabbix/web

rm -rf /run/zabbixsrv
rm -rf /var/mysql*
rm -rf /var/log/nginx /var/log/php-fpm /var/log/zabbix* /var/log/mysqld.log

# 更新服务
systemctl daemon-reload
# 设置成自动服务启动
systemctl enable nginx php-fpm mysqld zabbix-server zabbix-agent grafana-server

# 如果要显示中文提示
# localectl set-locale LANG=zh_CN.utf8

# 如果要安装mysql
mysqld --initialize-insecure --console
# 后续mysql
systemctl start mysqld
mysql -uroot -p
# 找不到密码就去翻 /var/log/mysql/mysql.error.log
# cat /etomorrow/logs/mysql.error.log |grep password
set password = 'mY123#pa4s';
use mysql;
set global log_bin_trust_function_creators = 0;

CREATE DATABASE IF NOT EXISTS `zbx_data` DEFAULT CHARACTER SET utf8mb4 COLLATE  utf8mb4_general_ci;
CREATE DATABASE IF NOT EXISTS `eto_data` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
CREATE DATABASE IF NOT EXISTS `gra_data` DEFAULT CHARACTER SET utf8mb4 COLLATE  utf8mb4_general_ci;

create user 'zabbix'@'%' identified with mysql_native_password by 'mY123#pa4s';
# create user 'zabbix'@'localhost' identified by 'mY123#pa4s';
grant all privileges on eto_data.* to 'zabbix'@'%';
grant all privileges on zbx_data.* to 'zabbix'@'%';
grant all privileges on gra_data.* to 'zabbix'@'%';

flush privileges;
# drop user 'zabbix'@'localhost';

# 导入监控数据库
mysql -u root -p eto_data < ~/eto_data.sql

# 导入zabbix数据库
# 6.0
# create database zbx_data character set utf8mb4 collate utf8mb4_bin;
# mysql -u root -p zbx_data < /usr/share/zabbix-mysql/schema.sql
# mysql -u root -p zbx_data < /usr/share/zabbix-mysql/images.sql
# mysql -u root -p zbx_data < /usr/share/zabbix-mysql/data.sql

# 6.2以上
gzip -d /usr/share/zabbix-sql-scripts/mysql/server.sql.gz 
mysql -u root -p zbx_data < /usr/share/zabbix-sql-scripts/mysql/server.sql

# 把程序复制进去
cp -r ~/eto.cn /usr/share/etomonitor
chown -R nginx.www /usr/share/etomonitor

# 目录授权
chmod 777 /usr/share/etomonitor/runtime/

# 初始化zabbix的用户密码
# UPDATE users SET passwd = '$2a$10$ZXIvHAEP2ZM.dLXTm6uPHOMVlARXX7cqjbhM6Fn0cANzkCQBWpMrS' WHERE username = 'Admin';

# 特殊情况下centos9无法连接SQLSERVER（如安全加固），按以下方式处理

# 提示error:0A00018A:SSL routines::dh key too small
# vim /etc/crypto-policies/back-ends/opensslcnf.config
# 修改第1行：CipherString = @SECLEVEL=2为 CipherString = @SECLEVEL=1

# 修改freetds.conf可能也行,取消openssl ciphers中的注释
# vim /etc/freetds.conf
# openssl ciphers = HIGH:!SSLv2:!aNULL:-DH

